//grab the first form - hopefully the only one

console.log("well, this worked");
var form = $('form')[0];

form.onsubmit = function (e) {
  // stop the regular form submission
  e.preventDefault();

  // collect the form data while iterating over the inputs
  var data = {};
  data['description'] = '|||\n|--|--|\n';
  for (var i = 0, ii = form.length; i < ii; ++i) {
    var input = form[i];
    if (input.name) {
      //data[input.name] = input.value;
      data['description'] = data['description']+'|**'+input.name+'**|'+input.value+'|\n';
    }
  }

  data['title'] = 'Just a test';

  // construct an HTTP request
  var xhr = new XMLHttpRequest();
  xhr.open(form.method, form.action, true);
  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  xhr.setRequestHeader('PRIVATE-TOKEN', 'GITLAB_ACCESS_TOKEN');
  

  // send the collected data as JSON
  xhr.send(JSON.stringify(data));

  xhr.onloadend = function () {
    window.location.replace("submitted.html")
  };
};

