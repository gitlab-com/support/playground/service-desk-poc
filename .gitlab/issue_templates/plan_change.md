<!--
PLEASE READ THIS!

Use this template if you'd like to request that a subscription be changed on a namespace on GitLab.com. This includes downgrading GitLab employee namespaces as part of offboarding, creating Bronze or Silver trials for customers, and any other reason a GitLab.com admin needs to manually change the subscription associated with a user or group namespace.

If you're requesting a Bronze and Silver trial for a prospect please ensure that they have already initiated a Gold trial on their namespace via the customers portal: https://customers.gitlab.com/trials/new?gl_com=true BEFORE opening this issue.

Title this issue using the following format:

Title: Plan Change Request for <Namespace>
-->

|     |     |
|---|---|
|**Link to GitLab.com Namespace**| |
|**Contact Email**|  |
|**SFDC Link**|  |
|**End Date**| |
|**Upgrade Plan To**|  |
|**Downgrade Plan To**|  |

**Approved By**:
<!-- Approval should usually come through your manager. In the case of trial extensions made by sales no approval is required. -->

<!-- ALL FIELDS ARE REQUIRED! -->
<!-- NO ASSIGNEE NECESSARY! -->

/confidential
/label ~"Plan Change Request"

