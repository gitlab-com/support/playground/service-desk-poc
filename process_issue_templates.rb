Dir[".gitlab/issue_templates/*"].each do |f|
  md_file = File.open(f)
  file_data = md_file.readlines.map(&:chomp)
  
  ## probably too cute, but it splits on the known directory and . and grabs just the template name
  html_file_name = f.split("gitlab/issue_templates/").last.split(".").first + ".html"

  html_form = ""
  html_form << "<html>\n"
  html_form << "<head>\n"
  html_form << "<meta charset='UTF-8'>\n"
  html_form << "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'>\n</head>\n"
  html_form << "<div class='container'>"
  html_form << "<p class='col-md-6'> <form method='POST' action='https://gitlab.com/api/v4/projects/19200186/issues'>\n"

  ## generate HTML content
  file_data.each do |line|
    if line[0] == "|"
      line.split("|").each do |entry|
        ## process form field
        field = entry.split("**").last
        if field.to_s.strip.length > 0 and field.to_s.tr("-","").length > 0
          html_form << "<label for=\"#{field}\">#{field}:</label><br>\n"
          html_form << "<input type=\"text\" name=\"#{field}\"><br/>\n"
        end
      end
    end    
    
  end
    
  html_form << "<input type='submit' value='Submit'>\n"
  html_form << "</form></p>\n"
  html_form << "</div>\n"
  html_form << "<script src='https://code.jquery.com/jquery-3.5.1.min.js' integrity='sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=' crossorigin='anonymous'></script>\n"
  html_form << "<script src='submit.js'></script>\n"
  html_form << "</html>"
  File.write("public/#{html_file_name}",html_form)

  md_file.close
end

