### Service Desk Forms: POC

The goal of this POC is to:
- automatically process `.gitlab/issue_templates/*` into HTML forms through a CI pipeline
- Expose those forms through GitLab pages
- Use those forms to create well forumlated issues through GitLab Service Desk
