file_name = "public/submit.js"

text = File.read(file_name)
new_contents = text.gsub(/GITLAB_ACCESS_TOKEN/, ENV['GITLAB_ACCESS_TOKEN'])

File.open(file_name,"w"){|file| file.puts new_contents}
